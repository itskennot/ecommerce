import React from 'react'
import { useLocation } from 'react-router-dom';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import MyOrders from '../pages/MyOrders';
import ProductDetails from '../pages/ProductDetails';
import Restore from './Restore';
import DeleteItems from '../pages/DeleteItems';
import EditProduct from '../pages/EditProduct';
import Archived from '../pages/Archived';
import CreateItem from '../pages/CreateItem';
import AdminDb from '../pages/AdminDb';
import PageNotFound from '../pages/PageNotFound';
import Home from '../pages/Home';
import Register from '../pages/Register';
import Login from '../pages/Login';
import Logout from '../pages/Logout';

import { AnimatePresence } from 'framer-motion';

function AnimatedRoute() {
    const location = useLocation();
  return (
    <AnimatePresence>
    <Routes location={location} key={location.pathname}>
        <Route path='/myorders/:_id' element={<MyOrders />} />
        <Route path='/product/:_id' element = {<ProductDetails />} />
        <Route path='/admin/restore/:_id' element ={<Restore />} />
        <Route path='/admin/delete/:_id' element ={<DeleteItems />} />
        <Route path='/admin/edit/:_id' element ={<EditProduct />} />
        <Route path='/admin/deleted' element ={<Archived />} />
        <Route path='/admin/addproduct' element ={<CreateItem />} />
        <Route path='/admin' element = {<AdminDb />} />
        <Route path='/*' element = {<PageNotFound />}/>
        <Route path='/'  element ={<Home />} />
       <Route path='/register' element={ <Register />} />
       <Route path='/login' element= { <Login />} />
       <Route path='/logout' element = {<Logout />} />
        </Routes>
        </AnimatePresence>
  )
}

export default AnimatedRoute