import React from 'react'

function SaleBanner() {
  

  let word = document.getElementsByTagName('i');
  let i = 0;
  
   function rotator() {
     
      word[i].style.display = 'none';
      i = (i + 1) % word.length; 
      word[i].style.display = 'initial';
     
   }
      function start(){
        setTimeout(function(){
          rotator();

          start();
        },3000)
      }  
      start()

  return (
    <div>
        <div className='w-full h-[40px] bg-[#000000] md:flex justify-center items-center px-4 text-gray-300 hidden'>
            <i className='text-xs font-poppins' style={{display: 'initial'}}>TAKE AN ADDTIONAL 30% OFF SALE <span className='underline underline-offset-1'>SHOP NOW</span></i>
            <i className='text-xs font-poppins'>Free Shipping and Returns</i>
       </div>
      
    </div>
  )
}

export default SaleBanner