
import Navbar from './components/Navbar';

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

import AnimatedRoute from './components/AnimatedRoute';
import Sidebar from './components/Sidebar';
import Footer from './components/Footer';



function App() {
  
  const [user, setUser] = useState(null);
  const [userID, setUserID]= useState(null);
  
  
  const unSetUser = () => {
    localStorage.clear();
    
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/user/`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then(data => {
        if(localStorage.getItem('token')!==null){
          setUser({
            id:data._id
          })
          setUserID(data.isAdmin)
        }else{
          setUser(null)
        }
      
      })
  }, [])

 
  return (
    <UserProvider value = {{user, setUser, unSetUser, userID}}>
        <Router>
        <Navbar />  
        <AnimatedRoute />
        <Sidebar />
        <Footer />
        </Router>
        </UserProvider>
    
  );
}

export default App;
