import React, { Fragment, useContext } from 'react'
import Carousel from '../components/Carousel'
//import product context
import { ProductContext } from '../ProductContext'
import Product from '../components/Product';
import { motion } from 'framer-motion'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
function Home() {
  //get products from product context
  const { products } = useContext(ProductContext);

  //get only mens & womens clothing
  const filteredProducts = products.filter((item) => {
    return  item.category.indexOf('women')  || item.category.indexOf('men') ;
  });
  
  return (
      

    <motion.div 
    initial ={{width: 0}}
          animate ={{width: "100%"}}
          exit ={{x:window.innerWidth, transition: {duration: 0.1}}}
    >
    <Fragment>

        <Carousel />
        <section className='py-16'>
        <div className='container mx-auto w-full 100v'>
          <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-[30px] max-w-sm mx-auto md:max-w-none md:mx-0'>
            {filteredProducts.map(product => {
              return <Product product={product} key={product._id}/>
            })}
          </div>
        </div>
        </section>
    </Fragment>
    </motion.div>
  )
}

export default Home