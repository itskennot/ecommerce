import React from 'react'
import EditProd from '../components/EditProd'

function EditProduct() {
  return (
    <div className='container mx-auto w-full 100v'>
    <EditProd />
    </div>
  )
}

export default EditProduct