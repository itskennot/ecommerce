import React from 'react';
import { Link } from 'react-router-dom';

function MyOrders() {
  return (
    <div className='container mx-auto w-full 100v'>
    <h1 className='text-center text-2xl mt-10 mb-10'>ALL PRODUCT</h1>
    <div className="">

<div className="overflow-auto rounded-lg shadow block">
  <table className="w-full">
    <thead className="bg-gray-50 border-b-2 border-gray-200">
    <tr>
      <th className="w-20 p-3 text-sm font-semibold tracking-wide text-left">ID</th>
      <th className="p-3 text-sm font-semibold tracking-wide text-left">userId</th>
      <th className="w-24 p-3 text-sm font-semibold tracking-wide text-left">productId</th>
      <th className="w-24 p-3 text-sm font-semibold tracking-wide text-left">quantity</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">subTotal</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">Status</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">Operation</th>
    </tr>
    </thead>
    <tbody className="divide-y divide-gray-100">
      
    </tbody>
  </table>
  
</div>
</div>
<div className='flex justify-center items-center mt-5'>

<button type="button" className=" text-white bg-black hover:bg-gray-800 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-full text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700"><Link to = '/'>BACK</Link></button>
</div>
</div>
  )
}

export default MyOrders