import React, {createContext, useState, useEffect} from 'react'


export const ProductContext = createContext();
const ProductProvider = ({children}) => {
    const [products, setProducts] = useState([]);


    
      useEffect(() => {
        const fetchData = async () => {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/product/retreive`);
        try {
          const data = await res.json();
          setProducts(data)
        
        }
        catch{
         return false
        }
        
        }
        fetchData()
      },[])
    




  return  <ProductContext.Provider value={{products}}>
    {children}
  </ProductContext.Provider>
}

export default ProductProvider;